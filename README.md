# WP Node
[![WP Node](https://img.shields.io/badge/Built%20For%20WordPress-%E2%93%A6-lightgrey.svg?style=flat-square)](https://github.com/py7hon/wp-node)

# Purpose

- WordPress Workflow consisting NodeJS, Gulp, Composer and SASS
- Using underscores to create very minimal theme base
- Processing SASS with Gulp
- Minifying the CSS with Gulp
- Setting required dependencies

# Installation

2. Install Modules
    a) gulp - https://www.npmjs.com/package/gulp-install
    b) gulp-uglify - https://www.npmjs.com/package/gulp-uglify
    c) gulp-clean-css - https://www.npmjs.com/package/gulp-clean-css
    d) gulp-concat - https://www.npmjs.com/package/gulp-concat
    e) gulp-sass - https://www.npmjs.com/package/gulp-sass
    f) browser-sync - https://www.npmjs.com/package/browser-sync
3. Update gulpfile.js with directory location.

###### 2018 Iqbal Rifai